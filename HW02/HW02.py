#!/usr/bin/env python
# coding: utf-8

# ## Home work #2.
# # <i><b>Numpy</b></i>

# In[1]:


# Import Libraries
import numpy as np
import pandas as pd

print("Numpy version: ", np.__version__)
print("Pandas version: ", pd.__version__)


# In[2]:


# Import Dataset
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
iris = np.genfromtxt(url, delimiter=',', dtype='object')
names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')


# In[3]:


type(iris)


# In[4]:


# Look at the Data
pd.DataFrame(iris).head()


# Видно, что последняя колонка содержит текст, следовательно, не может быть конвертирована в числовой формат

# In[5]:


pd.DataFrame(iris).describe()


# In[6]:


type(iris[0, 0])


# In[7]:


# Check convertation into numbers for numeric columns
pd.DataFrame(iris[:, :-1].astype('float')).head()


# In[8]:


# Look at the statistics
pd.DataFrame(iris[:, :-1].astype('float')).describe()


# ### 1. Извлечь колонку ‘species’

# In[9]:


species = iris[:, -1].astype('str')
print('Dimensions of <species>: ', species.shape)
print('Type of <species>: ', type(species))
print("Type of <species'> elements: ", type(species[0]))


# In[10]:


iris_x = iris[:, :-1].astype('float')
print('Dimensions of <iris_x>: ', iris_x.shape)
print('Type of <iris_x>: ', type(iris_x))
print("Type of <iris_x's> elements: ", type(iris_x[0, 0]))


# ### 2. Преобразовать первые 4 колонки в 2D массив

# iris_x является 2D массивом ( <class 'numpy.ndarray'> ) числовых объектов ( <class 'numpy.float64'> ) размерности 150 х 4

# ### 3. Посчитать mean, median, standard deviation по 1-й колонке

# In[11]:


mean = iris_x[:, 0].mean()
median = np.median(iris_x[:, 0])
sd = iris_x[:, 0].std()

print('mean = ', mean)
print('median = ', median)
print('standard deviation = ', sd)


# Альтернативно:

# In[12]:


print('mean = ', iris_x.mean(axis = 0)[0])
print('median = ', np.median(iris_x, axis = 0)[0])
print('standard deviation = ', iris_x.std(axis = 0)[0])


# ### 4. Вставить 20 значений np.nan на случайные позиции в массиве

# In[13]:


# Check if any <nan> values are there
np.sum(np.isnan(iris_x))


# Массив не содержит пропусков

# In[14]:


# Autodetection of array's dimensions
rows, cols = iris_x.shape
print('Number of rows: ', rows)
print('Number of columns: ',cols)

# Number of nan-s
N = 20
print('N = ', N)


# Для определения случайных позиций по двумерным индексам для начала спрямляем массив в строку, перенумеровав все элементы единственным индексом, который принимает значения от нуля до количества элементов минус один, а затем восстанавливаем пары индексов из одномерного списка, который получился. При этом данный список (массив) заполняем без повторений.

# In[15]:


# Array of the random positions
OneIX = np.random.choice(range(rows * cols), N, replace=False)

# Array of row indexes
i = OneIX // cols

# Array of column indexes
j = OneIX % cols


# Для изменения содержимого зададим новый массив на тот случай, если модификацию значений нужно будет повторять

# In[16]:


iris_nan = iris_x.flatten().reshape(rows, cols)
iris_nan[i, j] = np.nan


# In[17]:


# Check there are no overlappings
np.sum(iris_x != iris_nan)


# ### 5. Найти позиции вставленных значений np.nan в 1-й колонке

# In[18]:


ix_nan_0 = np.arange(rows)[np.isnan(iris_nan[:, 0])]
print("Nan-s' indexes for the first column: ", ix_nan_0)


# ### 6. Отфильтровать массив по условию значения в 3-й колонке > 1.5 и значения в 1-й колонке < 5.0

# In[19]:


iris_nan[(iris_nan[:, 2] > 1.5) & (iris_nan[:, 0] < 5.), :]


# ### 7. Заменить все значения np.nan на 0

# Для изменения содержимого зададим новый массив на тот случай, если модификацию значений нужно будет повторять

# In[20]:


iris_0 = iris_nan.flatten().reshape(rows, cols)


# In[21]:


np.sum(iris_0 == 0.0)


# Массив пока не содержит нулей

# In[22]:


iris_0[np.isnan(iris_0)] = 0


# In[23]:


# Check is there <nan>-s now?
print('Nan-s: ', np.sum(np.isnan(iris_0)))

# Check how many zeros are in the array now
print('Zeros: ', np.sum(iris_0 == 0))


# ### 8. Посчитать все уникальные значения в массиве и вывести их вместе с подсчитанным количеством

# In[24]:


# Get the unique items and their counts
uniqs, counts = np.unique(iris_0, return_counts=True)
print("Unique items : ", uniqs)
print("Counts       : ", counts)


# ### 9. Разбить массив по горизонтали на 2 массива

# In[25]:


# Calculate breaking index
break_ix = rows // 2


# In[26]:


iris_upper = iris_0[:break_ix, :]
iris_lower = iris_0[break_ix:, :]


# In[27]:


# Check dimensions
print('Dimensions og the upper part: ', iris_upper.shape)
print('Dimensions of the lower part: ', iris_lower.shape)


# ### 10. Отсортировать оба получившихся массива по 1-й колонке: 1-й по возрастанию, 2-й по убыванию

# In[28]:


iris_upper = np.sort(iris_upper, axis=0)


# In[29]:


iris_lower = np.sort(iris_lower, axis=0)[::-1, :]


# ### 11. Склеить оба массива обратно

# In[30]:


iris_stack = np.vstack([iris_upper, iris_lower])

# Check the dimensions
print("Concatenated array's dimensions: ", iris_stack.shape)


# ### 12. Найти наиболее часто повторяющееся значение в массиве

# In[31]:


most_frequent = uniqs[np.argmax(counts)]
print('The most frequent value is ', most_frequent)


# ### 13. Написать функцию, которая бы умножала все значения в колонке, меньше среднего значения в этой колонке, на 2, и делила остальные значения на 4. Применить к 3-й колонке

# In[32]:


def multiply(a):
    aa = a
    ix = (a < np.mean(a))
    aa[ix] = aa[ix] * 2
    ix = (a >= np.mean(a))
    aa[ix] = aa[ix] / 4
    return aa


# In[33]:


iris_stack[:, 2] = multiply(iris_stack[:, 2])


# In[ ]:




